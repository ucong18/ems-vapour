import Vapor
import Leaf
import FluentPostgreSQL

/// Called before your application initializes.
public func configure(
    _ config: inout Config,
    _ env: inout Environment,
    _ services: inout Services
) throws {

    /// Register routes to the router
    let router = EngineRouter.default()
    try routes(router)
    services.register(router, as: Router.self)

    let leafProvider = LeafProvider()
    try services.register(leafProvider)
    config.prefer(LeafRenderer.self, for: ViewRenderer.self)

    try services.register(FluentPostgreSQLProvider())

    let postgresqlConfing = PostgreSQLDatabaseConfig(
        hostname: "localhost",
        port: 5432,
        username: "macbook",
        database: "foodtracker",
        password: nil
    )
    
    services.register(postgresqlConfing)
    

    var migrations = MigrationConfig()
    migrations.add(model: User.self, database: .psql)
    services.register(migrations)
}
