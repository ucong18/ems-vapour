import Vapor

/// Register your application's routes here.
public func routes(_ router: Router) throws {
    
    router.get("/") { req in
        return "It Works"
    }
    
    let userController = UserController()
    router.get("users", use: userController.list)
    router.post("users", use: userController.create)
    
    let userControllerApi = UserControllerApi()
    
    router.get("api/users", use: userControllerApi.list)
    router.post("api/users", use: userControllerApi.create)
    router.patch("api/users", User.parameter, use: userControllerApi.update)
   // router.delete("api/users", User.parameter, use: userControllerApi.delete)
}
