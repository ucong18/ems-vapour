//
//  UserControllerApi.swift
//  App
//
//  Created by MacBook on 17/09/19.
//

import Foundation
import Vapor

final class UserControllerApi {
    
   
    func list(_ req: Request) throws -> Future<[User]> {
        return User.query(on: req).all()
    }
    
    func create(_ req: Request) throws -> Future<User> {
        return try req.content.decode(User.self).flatMap { user in
            return user.save(on : req)
        }
    }
    
    func update(_ req: Request) throws -> Future<User> {
        return try req.parameters.next(User.self).flatMap { user in
            return try req.content.decode(User.self).flatMap { newUser in
                user.username = newUser.username
                return user.save(on: req)
            }
        }
    }
    
//    func delete(_ req : Request) throws -> Future<User> {
//        return try req.parameters.next(User.self).flatMap { delUser in
//            return delUser.delete(on: req)
//        }.transform(to: .ok)
//    }
}
